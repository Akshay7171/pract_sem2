# Write a python program to read three numbers and find the smallest among them
# Sandeep Mishra (2012093)

num1 = int(input("Enter num 1: "))
num2 = int(input("Enter num 2: "))
num3 = int(input("Enter num 3: "))

## Method 1
print("using min function")
print(min(num1, num2, num3))

## Method 2
print("using conditions")
if num1 < num2 and num1 < num3:
    print(num1)
elif num2 < num3:
    print(num2)
else:
    print(num3)

## Method 3
print("using sorted function")
print(sorted([num1, num2, num3])[0])
