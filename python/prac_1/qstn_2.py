## Write a python program to swap two numbers without using third variable
## Sandeep Mishra (2012093)

num1 = int(input("Enter num 1: "))
num2 = int(input("Enter num 2: "))

print(num1)
print(num2)

print("after swap without third variable")

num1, num2 = num2, num1

print(num1)
print(num2)
