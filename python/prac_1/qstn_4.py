# Write a python program to read three numbers and if any two variables are equal, print that number
# Sandeep Mishra (2012093)

num1 = int(input("Enter num 1: "))
num2 = int(input("Enter num 2: "))
num3 = int(input("Enter num 3: "))

if num1 == num2 or num1 == num3:
    print(num1)
elif num3 == num2:
    print(num3)
else:
    print("all are different")
