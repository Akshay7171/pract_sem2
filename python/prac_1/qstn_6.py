# Write a python program to read three numbers and print them in ascending order
# (without using sort function)
# Sandeep Mishra (2012093)

num1 = int(input("Enter num 1: "))
num2 = int(input("Enter num 2: "))
num3 = int(input("Enter num 3: "))

if num1 > num2:
    num1, num2 = num2, num1
if num1 > num3:
    num1, num3 = num3, num1
if num2 > num3:
    num2, num3 = num3, num2

print(num1)
print(num2)
print(num3)
