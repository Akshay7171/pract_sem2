## Write a python program to read two numbers and find the sum of their cubes
## Sandeep Mishra (2012093)

num1 = int(input("Enter num 1: "))
num2 = int(input("Enter num 2: "))

result = num1**3 + num2**3

print(result)
