const currentDateTime = require('./date_time').currentDateTime;

const date = currentDateTime(); //returns current date

console.log(date.toTimeString());
console.log(date.toLocaleDateString());