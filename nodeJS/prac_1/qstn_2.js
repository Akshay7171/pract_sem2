const url = require("url");

const testUrl = "https://www.mywebsite.com/index.php?name=sandeep&dep=mca&roll_no=2012093";
// parse the given url using url module
const parsedUrl = url.parse(testUrl);
const queryStr = parsedUrl.query;

console.log(`${queryStr}\n`);

console.log(queryStr.split("&"));

// iterate over each query and print
queryStr.split("&").forEach(qry => {
    console.log(qry);
});